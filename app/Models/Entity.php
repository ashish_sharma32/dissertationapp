<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Validator;
use Illuminate\Support\Str;
use Request;
use Illuminate\Support\Facades\Storage;

class Entity extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fname',
        'lname',
        'email',
        'telephone',
    ];

  public function storeValidRules(){
    return $rules = [
      'fname' => 'required|min:2',
      'lname' =>'required|min:2',
      'email' =>'required|min:2',
      'telephone' =>'required|min:2',
    ];
  }

  public function storeUpdateRules(){
    return $rules = [
      'fname' => 'required|min:2',
      'lname' =>'required|min:2',
      'email' =>'required|min:2',
      'telephone' =>'required|min:2',
    ];
  }

  public function storeEntity(){
    $validator = Validator::make(Request::all(),$this->storeValidRules());
    if($validator->passes()){
      $entity = new Entity;
      $entity->fname = Request::get('fname');
      $entity->lname = Request::get('lname');
      $entity->email = Request::get('email');
      $entity->telephone = Request::get('telephone');
      $entity->save();
      return true;
    }
    else{
      return false;
    }
  }

  public function updateEntity($id){
    $validator = Validator::make(Request::all(),$this->storeValidRules());
    if($validator->passes()){

      $entity = Entity::find($id);
      $entity->fname = Request::get('fname');
      $entity->lname = Request::get('lname');
      $entity->email = Request::get('email');
      $entity->telephone = Request::get('telephone');
      $entity->save();
      return true;
    }
    else{
      return false;
    }
  }



}
