<?php

namespace App\Http\Controllers;

use App\Models\Entity;
use Illuminate\Http\Request;

class EntityController extends Controller
{
    /**
     * Display a listing of the entity.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     $entities = Entity::all();
     if($entities){
      return response()->json($entities);       
    }
    else{
      return response()->json([
        'message' => 'Empty Entity Set',
      ]);
    }
  }

    /**
     * Store a newly created entity in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $entity = new Entity;
      if($entity->storeEntity()){
        return response()->json([
          'message' => 'Success',
        ]);
      }
      else{
        return response()->json([
          'message' => 'Fail',
        ]);
      }
    }

    /**
     * Display the specified entity.
     *
     * @param  \App\Models\Entity  $entity
     * @return \Illuminate\Http\Response
     */
    public function show(Entity $entity)
    {
        //
    }

    /**
     * Update the specified entity in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Entity  $entity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $entityInstance = new Entity;
      if($entityInstance->updateEntity($id)){
                // return response()->json($entityInstance);
        return response()->json([
          'message' => 'Success',
        ]);
      }
      else{
        return response()->json([
          'message' => 'Fail',
        ]);
      }
    }

    /**
     * Remove the specified entity from storage.
     *
     * @param  \App\Models\Entity  $entity
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $entityInstance = Entity::find($id);
      if($entityInstance){
        $entityInstance->delete();
        return response()->json([
          'message' => 'Success',
        ]);
      }
      else{
        return response()->json([
          'message' => 'Fail',
        ]);
      }
    }
  }
