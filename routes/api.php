<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('entities/list', 'App\Http\Controllers\EntityController@index');

Route::post('entities/store', 'App\Http\Controllers\EntityController@store');

Route::post('entities/update/{entity}', 'App\Http\Controllers\EntityController@update');

Route::get('entities/delete/{entity}', 'App\Http\Controllers\EntityController@destroy');
